BUILDS=$(patsubst %/Dockerfile,build/%,$(wildcard */Dockerfile))
PUSHES=$(patsubst %/Dockerfile,push/%,$(wildcard */Dockerfile))

default: build

pull:
	docker pull alpine:latest
	docker pull gcr.io/distroless/static:nonroot

build: pull $(BUILDS)
push: $(PUSHES)

.PHONY: pull build push

# ---------------------------------------------------------------------

build/%: %
	docker build -t blacksquaremedia/golang:$< $</

push/%: % build/%
	docker push blacksquaremedia/golang:$<
